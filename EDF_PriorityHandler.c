
#include "EDF_PriorityHandler.h"
#include "console.h"

static QueueHandle_t edfQueue;

void EDF_PriorityHandler_Init() {

  // initialise the active task list
  EDF_TaskList_Init();
  // 2.1 create the queue: max size is EDF_MAX_TASKS

  // 2.2 create the priority handler task, stack size is
  // configMINIMAL_STACK_SIZE, priority is EDF_TASK_PRIORITY_SCHEDULER
  
  
  
}

void EDF_PriorityHandler(void *pvParameters) {
  EDF_Message received_message;
  EDF_Task *task_handle = NULL;

  while (1) {
    // 4.1 receive a message from the queue, use portMAX_DELAY to wait
    // indefinitely for a new message
 

    // 4.2 depending on the message type, use
    // EDF_TaskList_Deadline_Insert/EDF_TaskList_Remove to add or remove a task
    // from the active list









    // 4.3 notify the task who sent the message

  }
}

uint32_t EDF_Task_Create(EDF_Task *create_task) {
  // 5.1  create the task using "xTaskCreate", give create_task as
  // parameter, use configMINIMAL_STACK_SIZE as stack size, use
  // EDF_TASK_PRIORITY_MINIMUM as priority, store the handle in create_task




  // 5.2  suspend the newly created task


  // create a new message
  EDF_Message create_task_message = {EDF_Message_Create,
                                     xTaskGetCurrentTaskHandle(), create_task};

  // 5.3 send a create-message to the queue, use portMAX_DELAY to wait
  // indefinitely for an empty slot in the queue


  // 5.4 wait to be notified by the priority handler

  // 5.5 resume the newly created task


  return 1;
}

uint32_t EDF_Task_Delete(TaskHandle_t delete_task) {
  EDF_Message delete_task_message = {EDF_Message_Delete, delete_task, NULL};
  // 6.1 send a delete-message to the queue, use portMAX_DELAY to wait
  // indefinitely for an empty slot in the queue


  // 6.2 wait to be notified by the priority handler

  // 6.3 delete the task using "vTaskDelete"


  return 1;
}


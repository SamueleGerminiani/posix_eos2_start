#include "EDF_ActiveList.h"
#include "console.h"

#include <assert.h>
static struct EDF_TaskList_t activeList;

EDF_Task *EDF_Task_Allocate() {
  // 3.1 Allocate memory for a new EDF_Task

  // 3.2 initialise all fields





  // 3.3 return the new EDF_Task

}

bool EDF_Task_Free(EDF_Task *task_to_remove) {
  // Check input parameters are not NULL
  if (task_to_remove == NULL) {
    console_print("ERROR in EDF_Task_Free: one of the parameters was NULL.\n");
    return false;
  }

  // 3.4 free the memory used by the task


  return true;
}

void EDF_TaskList_Init() { LIST_INIT(&activeList); }

void EDF_TaskList_Deadline_Insert(EDF_Task *task_to_insert) {

  // 7.1  handle the base case: the list is empty, insert as first element of
  // the list. Remember to set the priority of the new task! The base priority
  // is EDF_TASK_PRIORITY_MINIMUM.






  // retrieve the first element of the list
  EDF_Task *iterator = LIST_FIRST(&activeList);
  // retrieve its priority
  uint32_t itr_priority = uxTaskPriorityGet(iterator->task_handle);

  // check if we have reached the maximum number of active tasks
  if ((itr_priority + 1) == EDF_TASK_PRIORITY_GENERATOR) {
    printf("ERROR: REACHED LIMIT OF NUMBER OF SCHEDULABLE TASKS! NOT "
           "INSERTING TASK");
    exit(1);
  }

  /*
   * Remember that the tasks already in the list are sorted by decreasing
   * priority (increasing deadline), the first task has the highest priority
   * (the closest deadline) Remember that some of the priorities of the tasks
   * already in the list must be updated Example: List =
   * [<Task2,p:3,d:200>,<Task1,p:2,d:500>,<Task3,p:1,d:800>] New insertion :
   * <Task4,p:?,d:700> New list = [<Task2,p:4,d:200>,<Task1,p:3,d:500>,
   * <Task4,p:2,d:700>, <Task3, p:1,d:800>] Priority changes: Task2: 3 -> 4
   * Task1: 2 -> 3
   * Task4: ? -> 2
   * Task3 priority remains unchanged
   * */
  itr_priority++;
  while (iterator != LIST_END(&activeList)) {

    if (task_to_insert->deadline < iterator->deadline) {
      // 7.2 Insert the new task

      // 7.3 remember to update the priority of the real task using
      // "vTaskPrioritySet"

      return;
    } else if (LIST_NEXT(iterator, pointers) == LIST_END(&activeList)) {
      // 7.4 remember to handle the case in which the new task has the lowest
      // priority: insertion as last element

      // 7.5 update the priorities


      return;
    }

    // increase the priority of the current task in the list
    vTaskPrioritySet(iterator->task_handle, itr_priority);
    itr_priority--;

    // 7.6 update the iterator with the next element in the list

  }

  // something wrong happened if the execution arrives to this point
  assert(0);
}

void EDF_TaskList_Remove(TaskHandle_t task_to_remove) {

  EDF_Task *iterator = LIST_FIRST(&activeList);
  uint32_t itr_priority = uxTaskPriorityGet(iterator->task_handle);

  while (iterator != LIST_END(&activeList)) {
    if (iterator->task_handle == task_to_remove) {
      // 8.1 remove from the list and free the memory associated with the
      // respective EDF_Task*


      return;
    }

    itr_priority--;

    // 8.2 update the priority of the current task in the list

    // 8.3 update the iterator with the next element in the list

  }

  // something wrong happened if the execution arrives to this point
  assert(0);
}


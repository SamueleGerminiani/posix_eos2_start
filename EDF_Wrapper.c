#include "EDF_Wrapper.h"
#include "EDF_PriorityHandler.h"
#include "console.h"

// static functions

/** \brief it creates a new task instance after each period
 * \param pvParameters new periodic task parameters
 */
static void taskGenerator(void *pvParameters);

/** \brief it simulates the execution of a task
 * \param pvParameters new task parameters
 */
static void genericTask(void *pvParameters);

/** \brief it stops the scheduler if a deadline is missed
 * \param th handle of the task to be checked
 */
static void checkDeadline(EDF_Task *th);

/** \brief simulated execution time of a task
 * \param th handle of the task to simulate
 */
static void simulateExecution(EDF_Task *th);

void EDF_PeriodicTaskCreate(PeriodicTaskParam *param) {
  // 11.1 create a task generator, give param as parameter,
  // configMINIMAL_STACK_SIZE as stack size, EDF_TASK_PRIORITY_GENERATOR as
  // priority


}

void taskGenerator(void *pvParameters) {

  while (1) {
    EDF_Task *generated_task=NULL;
    // 10.1 allocate memory for a new EDF_Task*
    
    // 10.2  retrieve param from pvParameters
   

    // 10.3 fill the new EDF_Task* with the information in param
  
  
  
  
  
  

    // put a new task in the active list
    EDF_Task_Create(generated_task);

    // 10.4 delay until the next period
 
  }
}

void genericTask(void *pvParameters) {
  // 9.1  retrieve EDF_Task* from pvParameters
  EDF_Task *myself=NULL;

  while (1) {
    // 9.2 print the starting message of the task, the current time (in ticks)
    // and the priority




    // simulate execution
    simulateExecution(myself);

    // 9.3 print the ending message of the task, the current time (in ticks) and
    // the priority




    // 9.4 delete the task after execution

  }
}

static void checkDeadline(EDF_Task *th) {
  if (th->deadline < xTaskGetTickCount()) {
    console_print("%s deadline missed!\n", th->task_name);
    vTaskEndScheduler();
  }
}
static void simulateExecution(EDF_Task *th) {
  TickType_t current_time = 0;
  TickType_t previous_tick = 0;
  TickType_t execution_time = th->executionTime;
  TickType_t start_time = 0;
  uint32_t execution_counter = 0;
  current_time = xTaskGetTickCount();
  previous_tick = current_time;
  start_time = current_time;

  while (execution_counter < execution_time) {
    checkDeadline(th);
    current_time = xTaskGetTickCount();
    if (current_time != previous_tick) {
      execution_counter++;
    }
    previous_tick = current_time;
  }
}


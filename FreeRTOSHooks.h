#ifndef FREERTOSHOOKS_H_
#define FREERTOSHOOKS_H_

#include "includes.h"

void vApplicationMallocFailedHook(void);
void vApplicationStackOverflowHook(xTaskHandle pxTask, signed char *pcTaskName);
void vApplicationIdleHook(void);

#endif
